import './bubbleBorder.css';

function BubbleBorder({isReversed}) {
    return (
        <div className={`bubble-border ${isReversed ? 'bubble-border_reversed' : 'bubble-border_normal'}`}/>
    );
}

export default BubbleBorder;
